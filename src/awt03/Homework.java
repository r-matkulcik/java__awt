package awt03;

/**
 * Created by hlbinamyslenia on 9.12.15.
 */

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Homework extends Frame implements WindowListener, MouseMotionListener, MouseListener {

    private String fileName = "/src/awt03/text.txt";
    private String fileContent;
    private String[][] points;

    private int size = 500;

    public Homework() throws IOException {
        setTitle("Java AWT Homework 3 - Róbert Matkulčík");
        setSize(size, size);
        setLayout(new FlowLayout());
        setResizable(false);

        fileContent = getFileContent(fileName);
        System.out.println(fileContent);

        points = getPoints(fileContent);

        this.addMouseMotionListener(this);
        this.addMouseListener(this);
        addWindowListener(this);
        setVisible(true);
    }


    private String getFileContent(String fileName) throws IOException {
        String filePath = System.getProperty("user.dir") + fileName;

        return new String(Files.readAllBytes(Paths.get(filePath)));
    }

    private String[][] getPoints(String content){
        String[] lines = content.split("\n");

        int ll = lines.length;

        String[][] points = new String[ll][3];

        for (int i = 0; i < ll; i++){
            points[i] = lines[i].split(" ");
        }
        return points;
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLACK);

        for (String[] point : points) {
            int x = Integer.parseInt(point[0]);
            int y = Integer.parseInt(point[1]);
            int r = Integer.parseInt(point[2]);
            g.drawOval(x, y, r*2, r*2);
            System.out.println(x + " " + y +" "+ r);
        }

    }
    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        int mX = (int) mouseEvent.getPoint().getX();
        int mY = (int) mouseEvent.getPoint().getY();

        setTitle(String.format("Súradnice myši sú %d a %d", mX, mY));
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        int mX = (int) mouseEvent.getPoint().getX();
        int mY = (int) mouseEvent.getPoint().getY();

        System.out.println(String.format("%dx%d", mX, mY));

//        painDot(mX, mY);
    }


    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}

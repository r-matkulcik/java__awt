package awt01;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Homework extends Frame implements WindowListener, MouseMotionListener, MouseListener {

    private String fileName = "/src/awt01/text.txt";
    private String fileContent;

    private int  j;
    private int  y;
    private String[] coordinate = new String[8];
    private int[] parseCoordinate = new int[coordinate.length];

    private int radius = 100; // Xpx radius of Oval
    private int size = 400;

    public Homework() throws IOException{
        setTitle("Java AWT Homework 1 - Róbert Matkulčík");

        setSize(size, size);
        setLayout(new FlowLayout());
        setResizable(false);

        this.addMouseMotionListener(this);
        this.addMouseListener(this);

        addWindowListener(this);
        fileContent = readTextFile(fileName);
        System.out.println(fileContent);

        parseCoordinate = cutter(fileContent);
        setVisible(true);



    }


    public static String readTextFile(String fileName) throws IOException {
        String input = (System.getProperty("user.dir") + fileName);

        String content = new String(Files.readAllBytes(Paths.get(input)));
        /* Space in the end of String*/
        content = content+ " ";
        return content;

    }

    /** STRING CUTTER + PARSER**/
    private int[] cutter(String content){
        for(int i=0; i< content.length(); i++){
            if (content.charAt(i) == ' ') {
                System.out.println(i);
                coordinate [y] = content.substring(j, i);
                System.out.println(coordinate[y]);
                parseCoordinate[y] = Integer.parseInt(coordinate[y]);
                y++;
                j = i + 1;
            }
        }
        return parseCoordinate;
    }


    public void paint(Graphics g) {
        Graphics2D ga = (Graphics2D)g;
        ga.setPaint(Color.black);
        ga.fillOval(parseCoordinate[0], parseCoordinate[1], parseCoordinate[2], parseCoordinate[3]);
        ga.setPaint(Color.red);
        ga.drawOval(parseCoordinate[4], parseCoordinate[5], parseCoordinate[6], parseCoordinate[7]);


    }



    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        int mX = (int) mouseEvent.getPoint().getX();
        int mY = (int) mouseEvent.getPoint().getY();

        setTitle(String.format("Súradnice myši sú %d a %d", mX, mY));
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        int mX = (int) mouseEvent.getPoint().getX();
        int mY = (int) mouseEvent.getPoint().getY();

        System.out.println(String.format("%dx%d", mX, mY));

//        painDot(mX, mY);
    }


    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}

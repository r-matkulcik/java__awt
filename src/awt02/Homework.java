package awt02;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;


public class Homework extends Frame implements WindowListener, MouseMotionListener, MouseListener {


    private int x,y;

    private int radius = 5; // Xpx radius of Oval
    private int size = 400;

    public Homework() throws IOException{
        setTitle("Java AWT Homework 2 - Róbert Matkulčík");

        setSize(size, size);
        setLayout(new FlowLayout());
        setResizable(false);

        this.addMouseMotionListener(this);
        this.addMouseListener(this);

        addWindowListener(this);
        setVisible(true);
    }
    public void  myColor(Graphics g){
        Graphics2D ga = (Graphics2D)g;
        ga.setPaint(Color.red);
        int x1 = suradnice[0][0];
        int y1 = suradnice[0][1];
        int x2 = suradnice[1][0];
        int y2 = suradnice[1][1];

        ga.fillRect(
                x1,
                y1,
                x2 - x1,
                y2 - y1
        );

    }

    public void myPaint(Graphics g) {
        Graphics2D ga = (Graphics2D)g;
        ga.setPaint(Color.black);

        int x1 = suradnice[0][0];
        int y1 = suradnice[0][1];
        int x2 = suradnice[1][0];
        int y2 = suradnice[1][1];

        ga.fillRect(
                x1,
                y1,
                x2 - x1,
                y2 - y1
        );

    }
    public void myOval(Graphics g) {
        Graphics2D ga = (Graphics2D)g;
        ga.setPaint(Color.red);
        ga.fillOval(x, y, radius, radius);


    }



    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        int mX = (int) mouseEvent.getPoint().getX();
        int mY = (int) mouseEvent.getPoint().getY();

        setTitle(String.format("Mouse coordinate: %d a %d", mX, mY));
    }
    //Point array
    private int click = 1;
    private int[][] suradnice = new int[3][2];

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        x = (int) mouseEvent.getPoint().getX();
        y = (int) mouseEvent.getPoint().getY();



        //What click is it?
        if (click==1) {
            // draw oval
            myOval(getGraphics());
            suradnice[0][0] = x;
            suradnice[0][1] = y;
            click = 2;
        } else if (click==2){
            // draw oval
            myOval(getGraphics());
            suradnice[1][0] = x;
            suradnice[1][1] = y;
            click = 3;
            // draw rectangle
            //TODO: I cannot create rectangle if user click-nbr.2 have "- position"! So how to fix this bug :(
            myPaint(getGraphics());
        } else if(click==3) {
            suradnice[2][0] = x;
            suradnice[2][1] = y;
            click = 1;
            if    ((suradnice[2][0]>suradnice[0][0])
                && (suradnice[2][0]<suradnice[0][0]+suradnice[1][0]-suradnice[0][0])
                && (suradnice[2][1]>suradnice[0][1])
                && (suradnice[2][1]<suradnice[0][1]+suradnice[1][1]-suradnice[0][1])
            ){
                myColor(getGraphics());
            }
        }


        System.out.println(String.format("%dx%d", x, y));

    }


    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
